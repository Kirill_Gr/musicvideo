//
//  MusicVideoTableViewCell.swift
//  MusicVideo
//
//  Created by Nik on 01/05/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit

class MusicVideoTableViewCell: UITableViewCell {

    var video: Video? {
        didSet {
            updateCell()
        }
    }
    
    @IBOutlet var musicImage: UIImageView!
    @IBOutlet var musicRank: UILabel!
    @IBOutlet var musicTitle: UILabel!
    
    func updateCell() {
        musicRank.text = String(video!.vRank)
        musicRank.font = UIFont.preferredFontForTextStyle(UIFontTextStyleSubheadline)
        musicTitle.text = video?.vName
        musicTitle.font = UIFont.preferredFontForTextStyle(UIFontTextStyleSubheadline)
        
        if video!.vImageData != nil {
            musicImage.image = UIImage(data: video!.vImageData!)
        }
        else {
            LoadVideoImage(video!, imageView: musicImage)
        }
    }
    
    func LoadVideoImage(video: Video, imageView: UIImageView) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let data = NSData(contentsOfURL: NSURL(string: video.vImageUrl)!)
            var img: UIImage?
            if data != nil {
                video.vImageData = data
                img = UIImage(data: data!)
            }
            dispatch_async(dispatch_get_main_queue()) {
                imageView.image = img
            }
        }
    }
}