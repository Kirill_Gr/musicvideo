//
//  SettingsTableViewController.swift
//  MusicVideo
//
//  Created by Nik on 04/05/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit
import MessageUI

class SettingsTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet var securitySwitch: UISwitch!
    @IBOutlet var qualitySwitch: UISwitch!
    @IBOutlet var videoCounter: UILabel!
    @IBOutlet var videoCounterSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        securitySwitch.on = NSUserDefaults.standardUserDefaults().boolForKey("isSecuritySwitchOn")
        let counter_value = NSUserDefaults.standardUserDefaults().integerForKey("videoCounterValue")
        videoCounterSlider.value = Float(counter_value)
        videoCounter.text = String(counter_value)
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            let mailViewController = configureMail()
            
            if MFMailComposeViewController.canSendMail() {
                self.presentViewController(mailViewController, animated: true, completion: nil)
            }
            else {
                mailAlert()
            }
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
    
    func configureMail() -> MFMailComposeViewController {
        let mailViewController = MFMailComposeViewController()
        mailViewController.mailComposeDelegate = self
        mailViewController.setToRecipients(["thisApp@test.me"])
        mailViewController.setSubject("Music Video App Feedback")
        mailViewController.setMessageBody("Hi developer,\n\n I would like to share the following feedback...\n", isHTML: false)
        return mailViewController
    }
    
    func mailAlert() {
        let alertController: UIAlertController = UIAlertController(title: "Alert", message: "No email account setup for phone!", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "Ok", style: .Default, handler: nil)
        alertController.addAction(okAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func touchSecuritySwitch(sender: UISwitch) {
        NSUserDefaults.standardUserDefaults().setBool(securitySwitch.on, forKey: "isSecuritySwitchOn")
    }
    
    @IBAction func videoCounterSliderValueChanged(sender: AnyObject) {
        let counter_value = Int(videoCounterSlider.value)
        NSUserDefaults.standardUserDefaults().setInteger(counter_value, forKey: "videoCounterValue")
        videoCounter.text = String(counter_value)
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
