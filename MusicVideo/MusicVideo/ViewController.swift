//
//  ViewController.swift
//  MusicVideo
//
//  Created by Nik on 28/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    // MARK: Constants
    
    // MARK: Variables
    private var content = [Video]()
    private var filteredContentTable = [Video]()
    private let resultSearchController = UISearchController(searchResultsController: nil)
    
    // MARK: Outlets
    @IBOutlet var contentTable: UITableView!
    
    // MARK: Properties
    
    // MARK: UIViewController Lifecycle
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "ReachStatusChanged", object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resultSearchController.searchResultsUpdater = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.reachabilityStatusChanged), name: "ReachStatusChanged", object: nil)
        reachabilityStatusChanged()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), forControlEvents: .ValueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: getDateString())
        contentTable.addSubview(refreshControl)
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        refreshControl.endRefreshing()
        if resultSearchController.active {
            refreshControl.attributedTitle = NSAttributedString(string: "No refresh allowed in search")
        }
        else {
            refreshControl.attributedTitle = NSAttributedString(string: getDateString())
            self.runApi()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "videoDetails" {
            if let indexPath = contentTable.indexPathForSelectedRow {
                let video: Video
                if resultSearchController.active {
                    video = filteredContentTable[indexPath.row]
                }
                else {
                    video = content[indexPath.row]
                }
                let target_view_controler = segue.destinationViewController as! VideoDetailsViewController
                target_view_controler.video = video
            }
        }
    }
    
    // MARK: Actions
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultSearchController.active {
            return filteredContentTable.count
        }
        return content.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! MusicVideoTableViewCell
        if resultSearchController.active {
            cell.video = filteredContentTable[indexPath.row]
        }
        else {
            cell.video = content[indexPath.row]
        }
        return cell
    }
    
    func runApi() {
        let limit = getVideosLimits()
        let api = APIManager()
        api.loadData("https://itunes.apple.com/us/rss/topmusicvideos/limit=\(limit)/json", onComplete: didLoadData)
    }
    
    func getVideosLimits() -> Int {
        var limit = NSUserDefaults.standardUserDefaults().objectForKey("videoCounterValue") as? Int
        if limit == nil {
            limit = 10
        }
        return limit!
    }
    
    func didLoadData(result: [Video]) {
        content = result
        title = "The iTunes Top \(content.count) Music Videos"
        
        //search controller setup
        definesPresentationContext = true
        resultSearchController.dimsBackgroundDuringPresentation = false
        resultSearchController.searchBar.placeholder = "Search for Artist"
        resultSearchController.searchBar.searchBarStyle = UISearchBarStyle.Prominent
        contentTable.tableHeaderView = resultSearchController.searchBar
        
        contentTable.reloadData()
    }

    func reachabilityStatusChanged() {
        switch reachabilityStatus {
            case NOACCESS:
                dispatch_async(dispatch_get_main_queue()) {
                    let alert = UIAlertController(title: "No internet access", message: "Please make sure you are connected to internet", preferredStyle: .Alert)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .Default) {
                        action -> () in
                        print("Cancel")
                    }
                    let deleteAction = UIAlertAction(title: "Delete", style: .Destructive) {
                        action -> () in
                        print("Delete")
                    }
                    let okAction = UIAlertAction(title: "Ok", style: .Default) {
                        action -> () in
                        print("Ok")
                    }
                    alert.addAction(cancelAction)
                    alert.addAction(deleteAction)
                    alert.addAction(okAction)
                }
            default:
                if content.count == 0 {
                    runApi()
                }
        }
    }
    
    func getDateString() -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "E, dd MMM yyyy HH:mm:ss"
        let dateString = formatter.stringFromDate(NSDate())
        return dateString
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let text = searchController.searchBar.text!
        filterSearch(text)
    }
    
    func filterSearch(searchText: String) {
        filteredContentTable = content.filter { element in
            return element.vArtist.lowercaseString.containsString(searchText.lowercaseString)
        }
        contentTable.reloadData()
    }
}

