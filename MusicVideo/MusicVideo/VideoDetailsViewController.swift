//
//  VideoDetailsViewController.swift
//  MusicVideo
//
//  Created by Nik on 03/05/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import LocalAuthentication

class VideoDetailsViewController: UIViewController {

    var video: Video!
    
    @IBOutlet var vName: UILabel!
    @IBOutlet var vImage: UIImageView!
    @IBOutlet var vGenre: UILabel!
    @IBOutlet var vPrice: UILabel!
    @IBOutlet var vRights: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = video.vArtist
        vName.text = video.vName
        vGenre.text = video.vGenre
        vPrice.text = video.vPrice
        vRights.text = video.vRights
        
        if video.vImageData != nil {
            vImage.image = UIImage(data: video.vImageData!)
        }
        else {
            vImage.image = UIImage(named: "no-image")
        }
    }
    
    @IBAction func playVideo(sender: UIBarButtonItem) {
        let player = AVPlayer(URL: NSURL(string: video.vVideoUrl)!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.presentViewController(playerViewController, animated: true) {
            playerViewController.player?.play()
        }
    }
    
    @IBAction func openSocialView(sender: UIBarButtonItem) {
        let isSecurityOn = NSUserDefaults.standardUserDefaults().boolForKey("isSecuritySwitchOn")
        if isSecurityOn == true {
            touchIdCheck()
        }
        else {
            createSocialActivities()
        }
    }

    func touchIdCheck() {
        let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "continue", style: UIAlertActionStyle.Cancel, handler: nil))
        
        let context = LAContext()
        var touchIdError: NSError?
        let reasonText = "Touch-Id authentication is needed to share info to Social Media"
        
        if context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &touchIdError) {
            
            context.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reasonText) {
                (success, policyError) -> Void in
                
                if success {
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                        self.createSocialActivities()
                    }
                }
                else {
                    alert.title = "Unsuccessful!"
                    switch LAError(rawValue: policyError!.code)! {
                    case .AppCancel : alert.message = "Authentication was cancelled by application"
                    case .AuthenticationFailed : alert.message = "The user failed to provide valid credentials"
                    case .PasscodeNotSet : alert.message = "Passcode is not set on the device"
                    case .SystemCancel : alert.message = "Authentication was cancelled by the system"
                    case .TouchIDLockout : alert.message = "Too many failed attempts"
                    case .UserCancel : alert.message = "You cancelled the request"
                    case .UserFallback : alert.message = "Password not accepted, must use touch-id"
                    default : alert.message = "Unable to athenticate!"
                    }
                    
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        else {
            alert.title = "Error"
            switch LAError(rawValue: touchIdError!.code)! {
            case .TouchIDNotEnrolled : alert.message = "TouchId is not enrolled"
            case .TouchIDNotAvailable : alert.message = "TouchId is not available on the device"
            case .PasscodeNotSet : alert.message = "Passcode has not been set"
            case .InvalidContext : alert.message = "The context is invalid"
            default : alert.message = "Local Authentication is not available"
            }
            
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    func createSocialActivities() {
        let activities: [String] = ["Have you had the opportunity to see this video?",
                                    "\(video.vName) by \(video.vArtist)",
                                    "Watch it and tell me what you think?",
                                    video.vLinkToStore]
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: activities, applicationActivities: nil)
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
}
