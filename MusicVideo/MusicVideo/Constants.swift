//
//  Constants.swift
//  MusicVideo
//
//  Created by Nik on 29/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: AnyObject]
typealias JSONArray = [AnyObject]

let WIFI = "WIFI available"
let NOACCESS = "No internet access"
let WWAN = "Cellular access available"