//
//  APIManager.swift
//  MusicVideo
//
//  Created by Nik on 28/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import Foundation

class APIManager {
    
    func loadData(urlString: String, onComplete: [Video] -> Void) {
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let url = NSURL(string: urlString)!
        let task = session.dataTaskWithURL(url) {
            (data, response, error) -> Void in
            
            if error != nil {
                print(error!.localizedDescription)
            }
            else {
                
                do {
                    if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? JSONDictionary,
                        feed = json["feed"] as? JSONDictionary,
                        vEntry = feed["entry"] as? JSONArray {
                    
                        var videos = [Video]()
                        for (index, element) in vEntry.enumerate() {
                            let video = Video(data: element as! JSONDictionary)
                            video.vRank = index + 1
                            videos.append(video)
                        }
                        
                        let priority = DISPATCH_QUEUE_PRIORITY_HIGH
                        dispatch_async(dispatch_get_global_queue(priority, 0)) {
                            dispatch_async(dispatch_get_main_queue()) {
                                onComplete(videos)
                            }
                        }
                    }
                }
                catch {
                    print("Error in JSONSerialization")
                }
            }
        }
        task.resume()
    }
}