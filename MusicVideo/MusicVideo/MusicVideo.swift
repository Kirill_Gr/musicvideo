//
//  MusicVideo.swift
//  MusicVideo
//
//  Created by Nik on 29/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import Foundation

class Video {
    
    var vRank = 0
    var vImageData: NSData?
    
    var vName: String {
        return _vName
    }
    var vImageUrl: String {
        return _vImageUrl
    }
    var vVideoUrl: String {
        return _vVideoUrl
    }
    var vRights: String {
        return _vRights
    }
    var vPrice: String {
        return _vPrice
    }
    var vArtist: String {
        return _vArtist
    }
    var vGenre: String {
        return _vGenre
    }
    var vReleaseDate: String {
        return _vReleaseDate
    }
    var vLinkToStore: String {
        return _vLinkToStore
    }
    
    private var _vName: String
    private var _vImageUrl: String
    private var _vVideoUrl: String
    private var _vRights: String
    private var _vPrice: String
    private var _vArtist: String
    private var _vGenre: String
    private var _vReleaseDate: String
    private var _vLinkToStore: String
    
    init(data: JSONDictionary) {
        
        if let imName = data["im:name"] as? JSONDictionary,
            label = imName["label"] as? String {
            
            _vName = label
        }
        else {
            _vName = ""
        }
        
        if let imImage = data["im:image"] as? JSONArray,
            img = imImage[2] as? JSONDictionary,
            imgUrl = img["label"] as? String {
            
            _vImageUrl = imgUrl.stringByReplacingOccurrencesOfString("100x100", withString: "600x600")
            
        }
        else {
            _vImageUrl = ""
        }
        
        if let link = data["link"] as? JSONArray,
            url = link[1] as? JSONDictionary,
            attributes = url["attributes"] as? JSONDictionary,
            href = attributes["href"] as? String {
            
            _vVideoUrl = href
        }
        else {
            _vVideoUrl = ""
        }
        
        if let rights = data["rights"] as? JSONDictionary,
            label = rights["label"] as? String {
        
            self._vRights = label
        }
        else {
            self._vRights = ""
        }
        
        if let imPrice = data["im:price"] as? JSONDictionary,
            label = imPrice["label"] as? String  {
        
            self._vPrice = label
        }
        else {
            self._vPrice = ""
        }
        
        if let imArtist = data["im:artist"] as? JSONDictionary,
            label = imArtist["label"] as? String  {
            
            self._vArtist = label
        }
        else {
            self._vArtist = ""
        }
        
        if let category = data["category"] as? JSONDictionary,
            attributes = category["attributes"] as? JSONDictionary,
            term = attributes["term"] as? String  {
            
            self._vGenre = term
        }
        else {
            self._vGenre = ""
        }
        
        if let id = data["id"] as? JSONDictionary,
            label = id["label"] as? String  {
            
            self._vLinkToStore = label
        }
        else {
            self._vLinkToStore = ""
        }
        
        if let imReleaseDate = data["im:releaseDate"] as? JSONDictionary,
            attributes = imReleaseDate["attributes"] as? JSONDictionary,
            label = attributes["label"] as? String  {
            
            self._vReleaseDate = label
        }
        else {
            self._vReleaseDate = ""
        }
        
    }
}